//
//  borderImage.swift
//  devprofiletry2
//
//  Created by luka on 24/12/2018.
//  Copyright © 2018 luka. All rights reserved.
//

import UIKit

class borderImage: UIImageView {

    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 20.0
    }

}
